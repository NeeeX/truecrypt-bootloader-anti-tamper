# Truecrypt Bootloader Anti-Tamper #

Truecrypt bootloader responsible for the volume decryption is stored in first 62 sectors of hard drive. The script will calculate a MD5 hash of following sectors and match it against value given by the user.
When the missmatch between calculated and provided value is detected, the MessageBox with the warning will be displayed - indicating that the bootloader code has been tampered with.

The best way to use this script is to have it automatically run on every Windows boot.