import md5
import ctypes
from optparse import OptionParser

num_sector_bytes 	= 512
num_sectors_check	= 62


parser = OptionParser()
parser.add_option("-d", dest="drive_num", action="store",
				  type="string", help ="Drive number", default="1")
parser.add_option("-p", dest="valid_hash", action="store", 
				  type="string", help="Valid hash of first sectors");
parser.add_option("-g", dest="print_hash", action="store_true",
				  default=False, help="Prints the current hash")

(options, args) = parser.parse_args()

if not options.print_hash and not options.valid_hash:
	parser.print_help()
	exit()

try:
	disk_str = "\\\\.\\PhysicalDrive" + options.drive_num
	mbr_data = open(disk_str).read(num_sectors_check * num_sector_bytes)
except IOError as err_msg:
	print err_msg
	exit()

if not mbr_data:
	print "Unable to read first track of the hard drive"
	exit()
else:
	m = md5.new()
	m.update(mbr_data)


if options.print_hash:
	print "Hash of Drive %s:" % (options.drive_num)
	print m.hexdigest()
	exit()

if m.hexdigest() !=  options.valid_hash:
	MessageBox = ctypes.windll.user32.MessageBoxA
	MessageBox(None, 'The MBR hashes does not match!', 'WARNING', 0)



